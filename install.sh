deactivate
rm -rf venv __pycache__

python3 -m venv venv
source venv/bin/activate

pip install --upgrade wheel setuptools
pip install -r requirements.txt