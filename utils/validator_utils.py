# -*- coding: utf-8 -*-
import re
from numbers import Number
import datetime as dt

def validate_rule(key, value, rule_value):
    ''' Validar uma determinada regra '''

    if key == 'array' and not isinstance(value, list):
        return "não é uma lista (array)"

    else:

        if (isinstance(value, list)):
            arr = value
        else:
            arr = [value]

        for value in arr:

            if key == 'string' and not isinstance(value, str):
                return "não é uma string"
            
            elif key == 'integer':
                
                try:
                    int(value)
                except:
                    return "não é um número inteiro"
            
            elif key == 'numeric':
                
                try:
                    float(value)
                except:
                    return "não é um valor númerico"
            
            elif key == 'boolean' and not isinstance(value, bool):
                    return "não é um valor booleano"

            elif key == 'datetime':
                
                if not isinstance(value, dt.datetime):
                    
                    if isinstance(value, str):
                        
                        date_format = '%Y-%m-%d %H:%M:%S'
                        
                        try:
                            dt.datetime.strptime(value, date_format)
                            
                        except ValueError:
                            return "não é uma data válida"
                        
                    else:
                        return "não é uma data (representada em texto) válida"

            elif key == 'date':
                    
                if isinstance(value, str):
                    
                    date_format = '%Y-%m-%d'
                    
                    try:
                        dt.datetime.strptime(value, date_format)
                        
                    except ValueError:
                        return "não é uma data válida"
                    
                else:
                    return "não é uma data (representada em texto) válida"

            elif key == 'time':

                try:
                    
                    parts = value.split(":")

                    if len(parts) == 2:
                        
                        hours = int(parts[0])
                        minutes = int(parts[1])

                        if ((hours < 0 or hours > 23) or (minutes < 0 or minutes > 59)):
                            raise Exception("Formato inválido")

                    else:
                        raise Exception("Formato inválido")

                except Exception:
                    return "não é um horário válido"
            
            elif key == 'minlength' or key == 'maxlength' or key == 'min' or key == 'max' or key == 'pattern':
                
                if rule_value == None:
                    return "deve possuir um valor de referência para a validação da regra '" + key + "'"
                
                else:
                    
                    if key == 'minlength' and len(str(value)) < int(rule_value):
                        return "deve possuir ao menos " + rule_value + " caracteres"
                    
                    elif key == 'maxlength' and len(str(value)) > int(rule_value):
                        return "deve possuir no máximo " + rule_value + " caracteres"
                    
                    elif key == 'pattern':
                        
                        pattern = re.compile(rule_value)
                        match = pattern.match(value)
                        
                        if bool(match) == False:
                            return "deve possuir um formato válido"

                    elif key == 'min' and float(value) < float(rule_value):
                        return "deve ser maior que " + rule_value
                        
                    elif key == 'max' and float(value) > float(rule_value):
                        return "deve ser menor que " + rule_value

def validate(data, rules):
    """ Validar dicionário com regras especificadas
    """
    
    errors = []
    
    for key in rules:
        
        krules = []
        temp = rules[key]
        
        if isinstance(temp, str):
            temp = temp.split('|')
        
        error_prefix = "O campo '" + key + "' "
        
        value_required = False
        
        for r in temp:
            
            temp2 = r.split(':')
            
            rule_item = {'name': temp2[0]}
            
            if (len(temp2) > 1):
                rule_item['value'] = r.replace(temp2[0] + ':', '')
            
            if (rule_item['name'] == 'required'):
                value_required = True
            
            else:
                krules.append(rule_item)
        
        key_parts = key.split('.')
        value = data
        
        for k in key_parts:
            
            if isinstance(value, list):
                
                old = value
                
                try:
                    value = value[int(k)]
                except:
                    value = None
                    
            else:
                value = value.get(k, None) # valor do dicionario que deve respeitar a regra
        
            if (value == None):
                break
        
        if value_required and value == None:
            errors.append(error_prefix + "é obrigatório")
            
        # Só é necessário validar as outras regras caso possua valor
        elif value:
            
            for rule in krules:
                
                k = rule.get('name') # regra a ser checada
                v = rule.get('value', None) # valor da regra a ser checada (ex: min:6)
                
                rule_error = validate_rule(k, value, v)
                
                if (rule_error):
                    errors.append(error_prefix + rule_error)
                    break
    
    return errors