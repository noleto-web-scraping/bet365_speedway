import apppath
from jproperties import Properties

cfg = None

def load_file():
    ''' Carregar arquivo '''

    # Obtêm a pasta raiz da aplicação
    base_path = apppath.get_base_path()

    # Path do arquivo de configuração
    config_path = base_path + "/env.conf"

    file = open(config_path, "rb")

    cfg = Properties()
    cfg.load(file, "utf-8")

    return cfg

def env(key, default = None):
    ''' Carregar arquivo de configuração'''

    global cfg

    if not cfg:
        cfg = load_file()

    key = key.upper()

    try:
        value = cfg[key].data

    # Caso o valor não exista, retornar um default
    except KeyError:
        value = default

    replaces = {
        "true": True,
        "false": False,
        "null": None,
        "none": None
    }

    if value in replaces:
        value = replaces[value]

    if isinstance(value, str) and value.isdecimal():
        value = int(value)

    return value