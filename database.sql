CREATE TABLE results (
	  id serial,
    event_time character varying(5) NOT NULL,
    event_datetime timestamp(0) NOT NULL,
    podium_1 int NOT NULL,
    podium_2 int NOT NULL,
    podium_3 int NOT NULL,
    podium_4 int NOT NULL,
    podium_list int[] NOT NULL,
    created_at timestamp(0) NOT NULL
);

CREATE TABLE odds (
  	id serial,
    event_time character varying(5) NOT NULL,
    event_datetime timestamp(0) NOT NULL,
    bike_1 float NOT NULL,
    bike_2 float NOT NULL,
    bike_3 float NOT NULL,
    bike_4 float NOT NULL,
    odds_list float[] NOT NULL,
    created_at timestamp(0) NOT NULL
);

CREATE OR REPLACE FUNCTION array_min(anyarray)
RETURNS anyelement
LANGUAGE SQL
AS $$
  SELECT min(elements) FROM unnest($1) elements
$$;

CREATE OR REPLACE FUNCTION array_max(anyarray)
RETURNS anyelement
LANGUAGE SQL
AS $$
  SELECT max(elements) FROM unnest($1) elements
$$;

CREATE VIEW event_times as
SELECT
	rank() over(order by event_time asc) as index,
	event_time
FROM (
	SELECT
		to_char(x::time, 'HH24:MI') as event_time
	FROM
	generate_series(timestamp '2000-01-01 00:01:00', timestamp '2000-01-01 23:59:59', interval '3 min') t(x)
) q