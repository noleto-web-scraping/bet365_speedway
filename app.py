# Native imports
import json
import sys
import time
from time import sleep
from datetime import datetime, timedelta

# Vendor imports
from psycopg2.pool import SimpleConnectionPool

# My imports
from utils.env_utils import env
from utils.log_utils import Log

db_pool = SimpleConnectionPool(
    minconn = env('db_minconn'),
    maxconn = env('db_maxconn'),
    host = env('db_host'),
    port = env('db_port'),
    dbname = env('db_database'),
    user = env('db_user'),
    password = env('db_password')
)

Log.init()

def __insert_results(results, utc_date):
    ''' Inserir resultados no banco de dados '''

    from models import result_model

    log_prefix = '[RESULTS][INSERT]'

    for idx, result in enumerate(results):

        try:

            ev_time = result['time']
            
            # Incrementar minutos
            idx_utc_date =  utc_date + timedelta(minutes = idx*3)

            # Data/hora correta do evento
            ev_datetime = idx_utc_date.strftime("%Y-%m-%d") + ' ' + ev_time + ':00'

            podium_list = [
                int(result['podium'][0]),
                int(result['podium'][1]),
                int(result['podium'][2]),
                int(result['podium'][3]),
            ]

            # Transformar [1, 2, 3, 4] em {1, 2, 3, 4}
            podium_list_str = '{' + ','.join(map(str, podium_list)) + '}'

            result_insert = {
                'event_time': ev_time,
                'event_datetime': ev_datetime,
                'podium_1': podium_list[0],
                'podium_2': podium_list[1],
                'podium_3': podium_list[2],
                'podium_4': podium_list[3],
                'podium_list': podium_list_str
            }

            Log.debug(log_prefix + ' Preparando para inserir resultado: ' + json.dumps(result_insert))

            insert_result = result_model.insert(result_insert)

            if insert_result:
                Log.info(log_prefix + ' Resultado inserido com sucesso no banco de dados: ' + ev_time)

        except Exception as e:
            Log.error(log_prefix + ' Não foi possível inserir o registro no banco de dados: ' + str(e))

def __insert_odds_results(results, utc_date):
    ''' Inserir odds futuras no banco de dados '''

    from models import odd_model

    log_prefix = '[ODDS][INSERT]'

    for idx, result in enumerate(results):

        try:

            ev_time = result['time']
            
            # Incrementar minutos
            idx_utc_date =  utc_date + timedelta(minutes = idx*3)

            # Data/hora correta do evento
            ev_datetime = idx_utc_date.strftime("%Y-%m-%d") + ' ' + ev_time + ':00'
	
            odds_list = [
                float(result['odds'][0]),
                float(result['odds'][1]),
                float(result['odds'][2]),
                float(result['odds'][3])
            ]

            # Transformar [1, 2, 3, 4] em {1, 2, 3, 4}
            odds_list_str = '{' + ','.join(map(str, odds_list)) + '}'
            
            result_insert = {
                'event_time': ev_time,
                'event_datetime': ev_datetime,
                'bike_1': odds_list[0],
                'bike_2': odds_list[1],
                'bike_3': odds_list[2],
                'bike_4': odds_list[3],
                'odds_list': odds_list_str
            }

            Log.debug(log_prefix + ' Preparando para inserir resultado: ' + json.dumps(result_insert))

            insert_result = odd_model.insert(result_insert)

            if insert_result:
                Log.info(log_prefix + ' Resultado inserido com sucesso no banco de dados: ' + ev_time)

        except Exception as e:
            Log.error(log_prefix + ' Não foi possível inserir o registro no banco de dados: ' + str(e))

def run_bot(method):
    ''' Rodar bot '''

    t0 = time.time()

    from bot import BetBot

    log_prefix = '[RESULTS][SEARCH]' if method == 'results' else '[ODDS][SEARCH]'

    # Iniciar instância do bot
    bot_instance = BetBot()

    utc_date = datetime.utcnow()

    bot_instance.driver.get('https://www.bet365.com/#/AVR/B24/R^1/')
			
    Log.info(log_prefix + ' Acessando a página às ' + str(datetime.now()))
    
    sleep(bot_instance.wait_post_click)

    results = None

    try:
        
        if method == 'results':
            results = bot_instance.get_results()

        elif method == 'odds':
            results = bot_instance.get_odds()

    except Exception as e:
        Log.error(log_prefix + ' Não foi possível concluir a leitura dos resultados: ' + str(e))

    t1 = time.time()
    Log.debug(log_prefix + ' Leitura dos resultados finalizada em ' + str(round(t1-t0, 2)) + 'seg')

    if results and len(results) > 0:

        try:

            if method == 'results':
                __insert_results(results, utc_date)

            elif method == 'odds':
                __insert_odds_results(results, utc_date)

        except Exception as e:
            Log.error(log_prefix + ' Não foi possível inserir os resultados: ' + str(e))

    t2 = time.time()
    Log.info(log_prefix + ' Processo finalizado em ' + str(round(t2-t0, 2)) + 'seg')
	
args = sys.argv

if len(args) > 1:

    if args[1] == 'results' or args[1] == 'odds':
        run_bot(args[1])