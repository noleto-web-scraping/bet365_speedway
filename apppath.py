import sys
import os
from pathlib import Path

def get_base_path():
    ''' Obtem a pasta raiz da aplicação '''

    # Determine if application is a script file or frozen exe
    if getattr(sys, 'frozen', False):
        base_path = os.path.dirname(sys.executable)
    
    elif __file__:
        base_path = str(Path(__file__).parent.absolute())

    return base_path